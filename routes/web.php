<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Templates
// Route::get('/', function () { return view('welcome'); });
// Route::get('/home', function () { return view('home'); });

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'HomeController@logout')->name('home');
Route::get('blog/create' , 'BlogController@create');
Route::get('blog/list' , 'BlogController@index');
Route::get('blog/list' , 'BlogController@index');
Route::post('blog/' , 'BlogController@store');
// Route::get('/post', 'BlogController@logout')->name('home');
// php artisan make:controller BlogController --resource

@extends('layouts.app')
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
@section('content') 
    <div class = "container" >
        <div class="col-lg-12">
            Blog Liost

        <table>
            <tr>
                <th>ID</th>
                <th>User ID</th>
                <th>Title</th>
                <th>Body</th>
            </tr>
            @foreach ($blog_lists['data'] as $element)
                <tr>
                    <td>
                        {!! $element['id'] !!} 
                    </td> 
                    <td>
                        {!! $element['user_id'] !!} 
                    </td>
                    <td>
                        {!! $element['title'] !!} 
                    </td>
                    <td>
                        {!! $element['body'] !!} 
                    </td> 
                </tr>
            @endforeach
        </table>

        </div>
    </div>
@endsection
@extends('layouts.app')
@section('content') 
    <div class = "container" >
        <div class="col-lg-12">
            <form method="POST" action="/blog" class="col-lg-12">
                <div>
                    <label>Title</label>
                    <input class="col-lg-12" type="text" name="title" placeholder="Title" id='title'>
                </div>
                <div>
                    <label>Body</label>
                    <textarea class="col-lg-12" type="text" name="body" placeholder="Blog Body" id='body'></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection